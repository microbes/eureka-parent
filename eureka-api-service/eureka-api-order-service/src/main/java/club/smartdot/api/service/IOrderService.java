package club.smartdot.api.service;

import org.springframework.web.bind.annotation.RequestMapping;

import club.smartdot.base.ResponseBase;

public interface IOrderService {
	
	// 订单服务调用会员服务接口信息 fegin
	@RequestMapping(value = "/orderToMember")
	public String orderToMember(String name);
	
	
	// 订单服务调用会员服务用户信息
	@RequestMapping(value = "/orderToMemberUserInfo")
	public ResponseBase orderToMemberUserInfo();
	
	
	// 订单服务接口
	@RequestMapping(value = "/orderInfo")
	public ResponseBase orderInfo();
}
