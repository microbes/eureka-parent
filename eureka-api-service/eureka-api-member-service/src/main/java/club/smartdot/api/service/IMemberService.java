package club.smartdot.api.service;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import club.smartdot.api.entity.UserEntity;
import club.smartdot.base.ResponseBase;

public interface IMemberService {

	// 实体类和定义接口信息存放在接口项目
	// 代码实现类存放在接口实现类例
	@RequestMapping("/getMember")
	public UserEntity getMember(@RequestParam("name") String name);
	
	@RequestMapping("/getUserInfo")
	public ResponseBase getUserInfo();
}
