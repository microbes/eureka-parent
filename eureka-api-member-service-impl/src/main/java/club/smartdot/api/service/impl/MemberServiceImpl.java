package club.smartdot.api.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import club.smartdot.api.entity.UserEntity;
import club.smartdot.api.service.IMemberService;
import club.smartdot.base.BaseApiService;
import club.smartdot.base.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "会员服务接口")
@RestController
public class MemberServiceImpl extends BaseApiService implements IMemberService {

	@Value("${server.port}")
	private String serverPort;

	@ApiOperation(value = "获取会员信息")
	@Override
	@RequestMapping("/getMember")
	public UserEntity getMember(String name) {
		UserEntity userEntity = new UserEntity();
		userEntity.setName(name + "; 当前端口:" + serverPort);
		userEntity.setAge(25);
		return userEntity;
	}

	@ApiOperation(value = "获取用户信息,休眠1.5s")
	@Override
	@RequestMapping("/getUserInfo")
	public ResponseBase getUserInfo() {
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return setResultSuccess("订单服务调用会员服务接口成功···" + "; 当前端口:" + serverPort);
	}

}
