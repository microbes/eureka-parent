package club.smartdot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
// @EnableEurekaServer表示开启EurekaServer服务,开启服务注册中心
@EnableEurekaServer
public class AppEureka {
	
	public static void main(String[] args) {
		SpringApplication.run(AppEureka.class, args);
	}
}
