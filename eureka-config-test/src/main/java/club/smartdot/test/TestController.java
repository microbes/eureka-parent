package club.smartdot.test;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class TestController {
	
	@Value("${custom.info}")
	private String customInfo;
	
	@RequestMapping(value = "getTestInfo")
	public String getTestInfo() {
		return customInfo;
	}
}
