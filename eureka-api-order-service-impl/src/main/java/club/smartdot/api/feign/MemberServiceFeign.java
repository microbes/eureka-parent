package club.smartdot.api.feign;

import org.springframework.cloud.openfeign.FeignClient;

import club.smartdot.api.fallback.MemberServiceFallback;
import club.smartdot.api.service.IMemberService;

@FeignClient(value = "app-member", fallback = MemberServiceFallback.class)
public interface MemberServiceFeign extends IMemberService {
	
}
