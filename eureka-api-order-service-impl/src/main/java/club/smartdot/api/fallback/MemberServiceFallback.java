package club.smartdot.api.fallback;

import org.springframework.stereotype.Component;

import club.smartdot.api.entity.UserEntity;
import club.smartdot.api.feign.MemberServiceFeign;
import club.smartdot.base.BaseApiService;
import club.smartdot.base.ResponseBase;

@Component
public class MemberServiceFallback extends BaseApiService implements MemberServiceFeign {

	@Override
	public UserEntity getMember(String name) {
		return null;
	}

	@Override
	public ResponseBase getUserInfo() {
		System.out.println("服务降级线程池名称:" + Thread.currentThread().getName());
		return setResultError("服务器忙,请稍后再试!以类的方式进行服务降级");
	}
	
}
