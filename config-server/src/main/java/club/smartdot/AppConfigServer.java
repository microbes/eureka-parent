package club.smartdot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EnableConfigServer
public class AppConfigServer {
	
	/***
	 * 如何把我们的配置文件存放在git环境上
	 * 1. 在git环境上创建配置文件名称规范: 服务名称-环境.properties
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(AppConfigServer.class, args);
	}
}
